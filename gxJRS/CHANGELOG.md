This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for gxJRS

## [v2.0.0-SNAPSHOT]

- removed all the old providers
- removed set of gcube headers
- removed isExtCall because authorization headers are not added automatically anymore

## [v1.2.0]

- Managing new UMA token and not only old authz gcube token [#21525]
- Switched JSON management to gcube-jackson [#19737]


## [v1.1.3] [r4.20.0] - 2019-10-19

- Allow clients to set secure protocol
- Switch from TLSv1 to TLSv1.2

  
## [v1.1.2] [r4.20.0] - 2019-10-19
 
- Migration to Git  


## [v1.1.1] [r4.0.0] - 2019-03-30
 
- Allow to set the media type for the returned entity in the Error Response.


## [v1.1.0] [r4.13.1] - 2019-02-26

- ...


## [v1.0.1] [r4.13.0] - 2018-11-20

- ...


## [v1.0.0] [r4.12.0] - 2018-06-20

- First Release