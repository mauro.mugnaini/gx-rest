gxHTTP
----
The gCube eXtensions to the HTTP protocol.
    
## Documentation
* [gxHTTP](https://wiki.gcube-system.org/gcube/GxRest/GxHTTP)
    
## Deployment
    
Notes about how to deploy this component on an infrastructure or link to wiki doc (if any).

## Built With

* [Jersey](https://jersey.github.io/) - JAX-RS runtime
* [Maven](https://maven.apache.org/) - Dependency Management

## License
See [LICENSE.md](LICENSE.md)